//
// Copyright (C) 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef DHCP_CLIENT_DBUS_SERVICE_DBUS_ADAPTOR_H_
#define DHCP_CLIENT_DBUS_SERVICE_DBUS_ADAPTOR_H_

#include <base/macros.h>

#include <dbus_bindings/org.chromium.dhcp_client.Service.h>

#include "dhcp_client/service_adaptor_interface.h"

namespace dhcp_client {

class Service;

class ServiceDBusAdaptor : public org::chromium::dhcp_client::ServiceAdaptor,
                           public org::chromium::dhcp_client::ServiceInterface,
                           public ServiceAdaptorInterface {
 public:
  ServiceDBusAdaptor(const scoped_refptr<dbus::Bus>& bus,
                     brillo::dbus_utils::ExportedObjectManager* object_manager,
                     Service* service);
  ~ServiceDBusAdaptor() override;

  // Implementation of ServiceAdaptorInterface.
  RPCObjectIdentifier GetRpcObjectIdentifier() override;
  void EmitEvent(const std::string& reason,
                 const brillo::VariantDictionary& configs) override;

 private:
  org::chromium::dhcp_client::ServiceAdaptor adaptor_;
  dbus::ObjectPath object_path_;
  brillo::dbus_utils::DBusObject dbus_object_;
  Service* service_;

  DISALLOW_COPY_AND_ASSIGN(ServiceDBusAdaptor);
};

}  // namespace dhcp_client

#endif  // DHCP_CLIENT_DBUS_SERVICE_DBUS_ADAPTOR_H_
