//
// Copyright (C) 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef DHCP_CLIENT_FILE_IO_H_
#define DHCP_CLIENT_FILE_IO_H_

#include <string>

#include <base/lazy_instance.h>
#include <shill/net/byte_string.h>

namespace dhcp_client {

// Singleton class for handling file operations.
class FileIO {
 public:
  virtual ~FileIO();

  // This is a singleton. Use FileWriter::GetInstance()->Foo().
  static FileIO* GetInstance();

  bool Write(const std::string& file_name,
             const shill::ByteString& data);

  bool Read(const std::string& file_name, shill::ByteString* out_data);

  bool PathExists(const std::string& file_name);
  bool DeleteFile(const std::string& file_name);

 protected:
  FileIO();

 private:
  friend struct base::DefaultLazyInstanceTraits<FileIO>;

  DISALLOW_COPY_AND_ASSIGN(FileIO);
};

}  // namespace dhcp_client

#endif  // DHCP_CLIENT_FILE_IO_H_
