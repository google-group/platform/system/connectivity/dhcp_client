//
// Copyright (C) 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef DHCP_CLIENT_SOCKET_UTIL_H_
#define DHCP_CLIENT_SOCKET_UTIL_H_

#include <string>

#include <base/macros.h>

namespace shill {

class ByteString;
class Sockets;

}  // namespace shill

namespace dhcp_client {

class SocketUtil {
 public:
  SocketUtil(const std::string& interface_name,
             unsigned int interface_index);
  virtual ~SocketUtil();
  // Create a raw socket and configure it with a UDP socket filter.
  bool CreateRawSocket(int* raw_socket);
  // Create a udp socket.
  bool CreateUdpSocket(int* udp_socket);
  // Send broadcast packet using a given raw socekt.
  bool SendBroadcastPacket(int raw_socket,
                           const shill::ByteString& buffer);
  // Send unicast packet using a given udp socekt.
  bool SendUnicastPacket(int udp_socket,
                         const shill::ByteString& buffer,
                         uint32_t server_ip);
 private:
  // Helper class with wrapped socket relavent functions.
  shill::Sockets* sockets_;
  std::string interface_name_;
  unsigned int interface_index_;

  DISALLOW_COPY_AND_ASSIGN(SocketUtil);
};

}  // namespace dhcp_client

#endif  // DHCP_CLIENT_SOCKET_UTIL_H_
