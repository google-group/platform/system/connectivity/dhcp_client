//
// Copyright (C) 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef DHCP_CLIENT_CONTROL_INTERFACE_H_
#define DHCP_CLIENT_CONTROL_INTERFACE_H_

#include <base/callback.h>
#include <base/macros.h>

#include "dhcp_client/manager_adaptor_interface.h"
#include "dhcp_client/service_adaptor_interface.h"

namespace dhcp_client {

class Manager;
class Service;

// This is the Interface for an object factory that creates adaptor/proxy
// objects
class ControlInterface {
 public:
  virtual ~ControlInterface() {}

  virtual void Init() = 0;
  virtual void Shutdown() = 0;

  // Adaptor creation APIs.
  virtual std::unique_ptr<ManagerAdaptorInterface> CreateManagerAdaptor(
      Manager* manager) = 0;
  virtual std::unique_ptr<ServiceAdaptorInterface> CreateServiceAdaptor(
      Service* service) = 0;


};

}  // namespace dhcp_client

#endif  // DHCP_CLIENT_CONTROL_INTERFACE_H_
